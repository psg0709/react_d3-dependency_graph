import React from "react";
import {withState, withHandlers, compose, StateHandler, StateHandlerMap} from 'recompose';
import * as R from "ramda";
import Graph from "./graph";
import * as gf from './graphFunctions';

const nodeColors = require('./color.json');
const data = require('./data.json');
const optionValues: number[] = [1, 2, 3, 4, 5];
const groups: string[] = ["projects", "datasets", "apps", "launchers", "modelAPI", "schedules", "people", "hardware", "environment"];

const INITIAL_STATE = {
    groups: {
        projects: true,
        datasets: true,
        apps: true,
        launchers: true,
        modelAPI: true,
        schedules: true,
        people: true,
        hardware: true,
        environment: true,
    },
    graphVisuals: {
        nodeLevel: 1,
        showDirectedLinksOnly: false,
    }
};

interface Groups {[id: string]: boolean};

interface GraphVisuals {
    nodeLevel: number,
    showDirectedLinksOnly: boolean,
};

interface State {
    groups: Groups;
    graphVisuals: GraphVisuals;
}

interface StateProps {
    onChangeGroup(val: string): void;
    state: State;
    onChange(val: number): void;
    onClick(): void;
    setState(setStateHandler: {(state: State): State}): void;
}

const GroupOptions = (props: {onChangeGroup(val: string): void}) =>
    <React.Fragment>
        {
            groups.map((value) =>
            <button key={value} value={value} onClick={() => props.onChangeGroup(value)}>{value}</button>)
        }
    </React.Fragment>

const options = optionValues.map((value) =>
    <option key={value} value={value.toString()}>{value}</option>);

const onChangeGroup = (props: StateProps) => (value: string) => props.setState((state: State) => {
    const newState = R.clone(state);
    newState.groups[value] = !newState.groups[value];
    return newState;
});

const onChange = (props: StateProps) => (value: number) => props.setState((state: State) => {
    const newState = R.clone(state);
    newState.graphVisuals.nodeLevel = value;
    return newState;
});

const onCLick = (props: StateProps) => () => props.setState((state: State) => {
    const newState = R.clone(state);
    newState.graphVisuals.showDirectedLinksOnly = !newState.graphVisuals.showDirectedLinksOnly;
    return newState;
});

const AppContent = (props: StateProps) =>
    <div>
        // @ts-ignore
        <Graph
            id="graph"
            width="1174"
            height="484"
            data={gf.filterData(R.clone(data), props.state.groups)}
            nodeColors={nodeColors}
            graphVisuals={props.state.graphVisuals}
        />
        <select
            onChange=
                {(e) => props.onChange(parseInt(e.target.value))} defaultValue=
                {props.state.graphVisuals.nodeLevel.toString()}
        >
            {options}
        </select>
        <GroupOptions onChangeGroup={props.onChangeGroup}/>
        <button onClick={() => props.onClick()}>
            {props.state.graphVisuals.showDirectedLinksOnly ? "All links" : "Directed links"}
        </button>
    </div>;

const App = compose(
    withState(
        'state', 'setState', INITIAL_STATE
    ),
    withHandlers({
        onChangeGroup: onChangeGroup,
        onChange: onChange,
        onClick: onCLick,
    })
    // @ts-ignore
)(AppContent);

export default App;
