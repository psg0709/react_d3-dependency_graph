import React, {Component} from 'react';
import {withState, lifecycle, withHandlers, compose, StateHandler, StateHandlerMap} from 'recompose';
import styled from 'styled-components';
import './graph.css';
import * as d3 from "d3";
import * as gf from "./graphFunctions";
import * as R from 'ramda';

const DATA: string = "data", GRAPH_VISUALS: string = "graphVisuals";

const GraphView = styled.div<graphView>`
  background-color: ${props => props.color};
  height: ${props => `${props.height}px`};
  width: ${props => `${props.width}px`};
`;

interface graphView {
    height: string;
    width: string;
}

interface Nodes {
    id: string;
    group: string;
    projects: string;
    value: number;
    risk: boolean;
}

interface Links {
    source: string,
    target: string[],
}

interface Data {
    nodes: Nodes[],
    links: Links[],
}

interface GraphVisuals {
    nodeLevel: number;
    showDirectedLinksOnly: boolean;
}

interface NodeColors { [id: string]: string };

interface Props {
    id: string;
    width: string;
    height: string;
    data: Data;
    nodeColors: NodeColors[];
    graphVisuals: GraphVisuals;

    graphRenderer(graphId: string, nodeColors: NodeColors[], graphVisuals: GraphVisuals, data: Data, changeIn: string): void;
}

interface NodeIndex { [id: string]: number };

interface SingleSrcTrg { source: string, target: string };

interface SvgNodes { innerCircle: {}, outerCircle: {} };

interface State {
    data: Data;
    nodeColors: NodeColors;
    nodeIndex: NodeIndex;
    modifiedLinks: SingleSrcTrg[];
    svgLinks: [];
    undirectedLinks: Links[];
    svgNodes: SvgNodes;
    graphVisuals: GraphVisuals;
}

interface StateHandlers {
    setGraph(setGraphStateHandler: {(state: State): State}): void;
}

const graphRenderer = (props: (Props & StateHandlers)) => (graphId: string, nodeColors: {}, graphVisuals: GraphVisuals, data: Data, changeIn: string) => props.setGraph((state: State) => {
    let newState = R.clone(state);
    if (R.equals(changeIn, DATA)) {
        newState.data = data;
        newState.nodeColors = nodeColors;
        newState.graphVisuals = graphVisuals;

        const svg = d3.select(graphId),
            svgWidth = +svg.attr("width"),
            svgHeight = +svg.attr("height");

        let simulation: {};
        simulation = d3.forceSimulation()
            .force("link", d3.forceLink().id((d: { [key: string]: any }) => d.id)
                .distance((d: { [key: string]: any }) => 50 + d.source.value + d.target.value)) // min distance to be maintained between two nodes
            .force("collisionForce", d3.forceCollide((d: { [key: string]: any }) => d.value + 50) // collision force between two nodes to avoid overlapping
                .strength(1))
        newState.nodeIndex = gf.initialiseNodeIndex(newState.data.nodes);
        newState.modifiedLinks = gf.splitLinks(newState.data.links);
        newState.undirectedLinks = gf.createUndirectedLinks(R.clone(newState.data.links), newState.nodeIndex);
        newState.svgLinks = gf.createSvgLinks(svg, newState.modifiedLinks);
        newState.svgNodes = gf.createSvgNodes(svg, newState.data.nodes, nodeColors);
        gf.createNodeTooltip(newState.svgNodes);
        gf.simulationSettings(simulation, newState.data.nodes, newState.svgNodes, newState.svgLinks, newState.modifiedLinks, svgWidth, svgHeight);
        gf.addMouseEvents(newState.svgNodes, newState.svgLinks, (graphVisuals.showDirectedLinksOnly ? newState.data.links : newState.undirectedLinks), newState.nodeIndex, graphVisuals.nodeLevel);
    }
    if (R.equals(changeIn, GRAPH_VISUALS)) {
        const svgNodes = state.svgNodes;
        const svgLinks = state.svgLinks;
        const undirectedLinks = state.undirectedLinks;
        const nodeIndex = state.nodeIndex;
        const data = state.data;
        gf.addMouseEvents(svgNodes, svgLinks, (graphVisuals.showDirectedLinksOnly ? data.links : undirectedLinks), nodeIndex, graphVisuals.nodeLevel);
        newState.graphVisuals = graphVisuals;
    }
    return newState;
})

const GraphContent = (props:Props) =>
    <GraphView color='white' width={props.width} height={props.height}>
        <svg id={props.id} width={props.width} height={props.height}>
            <defs>
                <radialGradient id="double-circle">
                    <stop offset="1" stopColor="white"/>
                    <stop stopColor="red"/>
                </radialGradient>
                <marker id="arrow-red"
                        viewBox="0 0 10 10"
                        refX="10" refY="5"
                        markerWidth="15"
                        markerHeight="15"
                        orient="auto"
                >
                    <path d="M 0 0 L 10 5 L 0 10 z" fill="#E64646"/>
                </marker>
                <marker id="arrow"
                        viewBox="0 0 10 10"
                        refX="10" refY="5"
                        markerWidth="15"
                        markerHeight="15"
                        orient="auto"
                >
                    <path d="M 0 0 L 10 5 L 0 10 z" fill="#868686"/>
                </marker>
            </defs>
        </svg>
    </GraphView>;

const Graph = compose(
    withState(
        'state', 'setGraph', {
            data: {
                nodes: [],
                links: [],
            },
            nodeColors: {},
            nodeIndex: {},
            modifiedLinks: [],
            svgLinks: [],
            undirectedLinks: [],
            svgNodes: {innerCircle: {}, outerCircle: {}},
            graphVisuals: {
                nodeLevel: 0,
                showDirectedLinksOnly: false,
            },
        }
    ),
    withHandlers({
        graphRenderer: graphRenderer
    }),
    lifecycle({
        componentWillReceiveProps(nextProps: Props) {
            if (!R.equals(this.props, nextProps)) {
                const graphId: string = "#" + nextProps.id;
                const nodeColors: { [id: string]: string }[] = nextProps.nodeColors;
                const graphVisuals: GraphVisuals = nextProps.graphVisuals;
                const data: Data = R.clone(nextProps.data);
                let changeIn: string = "";

                if (!(R.equals(this.props.data, data) && R.equals(this.props.nodeColors, nodeColors))) {
                    d3.select(graphId).selectAll("g").remove();
                    changeIn = DATA;
                } else if (!(R.equals(this.props.graphVisuals, graphVisuals))) {
                    changeIn = GRAPH_VISUALS;
                }

                if (!R.equals(changeIn, ""))
                    this.props.graphRenderer(graphId, nodeColors, graphVisuals, data, changeIn);
            }
        },
        componentDidMount() {
            const graphId: string = "#" + this.props.id;
            const nodeColors = this.props.nodeColors;
            const graphVisuals = this.props.graphVisuals;
            const data = R.clone(this.props.data);
            const changeIn = DATA;
            this.props.graphRenderer(graphId, nodeColors, graphVisuals, data, changeIn);
        }
    })
    // @ts-ignore
)(GraphContent);

export default Graph;
