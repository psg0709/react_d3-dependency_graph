import * as R from "ramda";

const nodeInnerCircleStrokeWidth = 2;
const nodeOuterCircleStrokeWidth = 4;
const red = "#E64646";
const grey = "#868686";
const lineStrokeWidth = 0.7;

interface Nodes {
    id: string;
    group: string;
    projects: string;
    value: number;
    risk: boolean;
}

interface Data {
    nodes: Nodes[],
    links: Links[],
}

interface Links {
    source: string,
    target: string[],
}

interface GeneralList {
    [id: string]: any
};

interface SvgNodes {
    innerCircle: GeneralList;
    outerCircle: GeneralList;
}

export function filterData(data: Data, list: GeneralList) {
    const newData = R.clone(data);
    const includeNode = (node: Nodes) => list[node.group];
    newData.nodes = R.filter(includeNode, newData.nodes);

    // Used for storing index of each node id in data.nodes[]
    const nodeIndex = initialiseNodeIndex(data.nodes);

    const includeLinkSource = (link: Links) => list[data.nodes[nodeIndex[link.source]].group];
    newData.links = R.filter(includeLinkSource, newData.links);

    const includeTarget = (target: string) => list[data.nodes[nodeIndex[target]].group];
    newData.links.forEach((link, index) => {
        newData.links[index].target = R.filter(includeTarget, link.target);
    })
    return newData;
}

export function initialiseNodeIndex(nodes: Nodes[]) {
    let nodeIndexList: GeneralList = {};
    nodes.forEach(function (d, index) {
        nodeIndexList[d.id] = index;
    });
    return nodeIndexList;
}

export function simulationSettings(simulation: GeneralList, nodes: Nodes[], svgNodes: SvgNodes, svgLinks: GeneralList, modifiedLinks: {}, svgWidth: number, svgHeight: number) {
    simulation
        .nodes(nodes)
        .on("tick", () => ticked(svgLinks, svgNodes, svgWidth, svgHeight));

    simulation
        .force("link")
        .links(modifiedLinks);
}

//  find x co-ordinate on a line connecting (x1,y1) to (x2,y2) at a distance of d_t from (x1,y1)
export function find_x(x1: number, y1: number, x2: number, y2: number, d_t: number) {
    const d = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    const t = d_t / d;
    return (1 - t) * x1 + t * x2;
}

//  find y co-ordinate on a line connecting (x1,y1) to (x2,y2) at a distance of d_t from (x1,y1)
export function find_y(x1: number, y1: number, x2: number, y2: number, d_t: number) {
    const d = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    const t = d_t / d;
    return (1 - t) * y1 + t * y2;
}

export function ticked(svgLinks: GeneralList, svgNodes: SvgNodes, svgWidth: number, svgHeight: number) {
    svgNodes["innerCircle"]
        .attr("cx", (d: GeneralList) => {
            d.x = Math.max(d.value + 5, Math.min(svgWidth - (d.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth), d.x));
            return d.x;
        })
        .attr("cy", (d: GeneralList) => {
            d.y = Math.max(d.value + 5, Math.min(svgHeight - (d.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth), d.y));
            return d.y;
        });

    svgNodes["outerCircle"]
        .attr("cx", (d: GeneralList) => {
            d.x = Math.max(d.value + 5, Math.min(svgWidth - (d.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth), d.x));
            return d.x;
        })
        .attr("cy", (d: GeneralList) => {
            d.y = Math.max(d.value + 5, Math.min(svgHeight - (d.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth), d.y));
            return d.y;
        });

    svgLinks
        .attr("x1", (d: GeneralList) => find_x(d.source.x, d.source.y, d.target.x, d.target.y, d.source.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth))
        .attr("y1", (d: GeneralList) => find_y(d.source.x, d.source.y, d.target.x, d.target.y, d.source.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth))
        .attr("x2", (d: GeneralList) => find_x(d.target.x, d.target.y, d.source.x, d.source.y, d.target.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth))
        .attr("y2", (d: GeneralList) => find_y(d.target.x, d.target.y, d.source.x, d.source.y, d.target.value + nodeInnerCircleStrokeWidth + nodeOuterCircleStrokeWidth))
        .attr("stroke", (d: GeneralList) => d.source.risk & d.target.risk ? red : grey)
        .attr("marker-end", (d: GeneralList) => d.source.risk ? "url(#arrow-red)" : "url(#arrow)");
}

export function createNodeTooltip(svgNodes: SvgNodes) {
    svgNodes["innerCircle"].append("title")
        .text((d: GeneralList) => d.id);
    svgNodes["outerCircle"].append("title")
        .text((d: GeneralList) => d.id);
}

export function splitLinks(links: Links[]) {
    let modified_links: { source: string, target: string }[] = [];
    links.map((linkWithMultipleTarget) => {
        linkWithMultipleTarget.target.map((linkWithSingleTarget) => {
            let new_link: { source: string, target: string } = R.dissoc('target', R.clone(linkWithMultipleTarget));
            new_link.target = linkWithSingleTarget;
            modified_links.push(new_link);
        })
    });
    return modified_links;
}

export function createUndirectedLinks(links: Links[], nodeIndexList: { [id: string]: number }) {
    let undirectedLinks = links;
    undirectedLinks.map((link) => {
        link.target.map((target) => {
            undirectedLinks[nodeIndexList[target]].target.push(link.source)
        })
    })
    return undirectedLinks;
}

export function createSvgNodes(svg: any, nodes: Nodes[], nodeColors: GeneralList) {
    let node: SvgNodes = {innerCircle: {}, outerCircle: {}};

    node["outerCircle"] = svg.append("g")
        .attr("class", "nodes")
        .selectAll("g")
        .data(nodes)
        .enter()
        .append("circle")
        .attr("class", "outerCircle")
        .attr("stroke", (d: any) => d.risk ? red : "white")
        .attr("stroke-width", nodeOuterCircleStrokeWidth)
        .attr("r", (d: any) => d.value + nodeInnerCircleStrokeWidth - 1)
        .attr("cx", (d: any) => d.x)
        .attr("cy", (d: any) => d.y)
        .attr("fill", (d: any) => nodeColors[d.group]);

    node["innerCircle"] = svg.append("g")
        .attr("class", "nodes")
        .selectAll("g")
        .data(nodes)
        .enter()
        .append("circle")
        .attr("class", "innerCircle")
        .attr("stroke", "white")
        .attr("stroke-width", nodeInnerCircleStrokeWidth)
        .attr("r", (d: any) => d.value)
        .attr("cx", (d: any) => d.x)
        .attr("cy", (d: any) => d.y)
        .attr("fill", "none");

    return node;
}

export function createSvgLinks(svg: any, modifiedLinks: { source: string, target: string }[]) {
    return svg.append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(modifiedLinks)
        .enter().append("line")
        .attr("stroke-width", lineStrokeWidth)
}

export function addMouseEvents(svgNodes: SvgNodes, svgLinks: GeneralList, links: Links[], nodeIndexList: { [id: string]: number }, nodeLevel: number) {
    svgNodes["innerCircle"].on('mouseover', (d: GeneralList) => showConnectedNodesAndLinks(d, svgNodes, svgLinks, links, nodeIndexList, nodeLevel))
        .on('mouseout', (d: GeneralList) => showAllNodesAndLinks(svgNodes, svgLinks));
    svgNodes["outerCircle"].on('mouseover', (d: GeneralList) => showConnectedNodesAndLinks(d, svgNodes, svgLinks, links, nodeIndexList, nodeLevel))
        .on('mouseout', (d: GeneralList) => showAllNodesAndLinks(svgNodes, svgLinks));
}

export function showConnectedNodesAndLinks(d: any, svgNodes: SvgNodes, svgLinks: GeneralList, links: Links[], nodeIndexList: { [id: string]: number }, nodeLevel: number) {
    let activeNodes: { [id: string]: boolean } = bfs(nodeLevel, d.id, links, nodeIndexList);
    svgNodes["innerCircle"].attr("opacity", function (o: GeneralList) {
        return activeNodes[o.id] ? 1 : 0.2;
    });
    svgNodes["outerCircle"].attr("opacity", function (o: GeneralList) {
        return activeNodes[o.id] ? 1 : 0.2;
    });

    svgLinks.attr("opacity", function (o: GeneralList) {
        return activeNodes[o.source.id] && activeNodes[o.target.id] ? 1 : 0.2;
    });

    activeNodes = {};
}

export function showAllNodesAndLinks(svgNodes: SvgNodes, svgLinks: GeneralList) {
    svgNodes["innerCircle"].attr("opacity", 1);
    svgNodes["outerCircle"].attr("opacity", 1);
    svgLinks.attr("opacity", 1);
}

export function bfs(level: number, rootNode: string, links: Links[], nodeIndexList: GeneralList) {
    let activeNodes: { [id: string]: boolean } = {};
    let parent: string[] = [rootNode], child: string[] = [];
    while ((level >= 0) && (R.length(parent) > 0)) {
        parent.map((node) => {
            if (activeNodes[node] === undefined) {
                activeNodes[node] = true;
                child = R.concat(child, links[nodeIndexList[node]].target);
            }
        })
        parent = child;
        child = [];
        level = level - 1;
    }
    return activeNodes;
}
