This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Data information

In the project data is passed to d3-graph using json format.
## Main files:

## `src/data.json`

Consists the data points to be passed for graph plotting. It has two entries ie. "nodes" and "links".

``"nodes"`` consists of the information related to individual data point such as id (or Data point unique name),
group (used for generating group specific node color), value (the weightage of the node, used for node radius)
and risk.

eg., ```{"id": "A", "group": "projects", "value": 25, "risk": 1}```

``"links"`` consists of single source and multiple targets array. source field should be one of the nodes given.
target field should be either array of nodes or an empty array.

eg., ```{"source": "A","target":["B","C","D","E"]}```
    ```{"source": "B","target":[]}```

## `src/color.json`

Consits group specific node colors.

eg., ```"projects":  "red"```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
